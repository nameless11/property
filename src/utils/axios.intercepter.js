import axios from "axios";

axios.interceptors.request.use(
  function (config) {
    let token = localStorage.getItem("token");

    !!token && (config.headers.Authorization = `Bearer ${token}`);
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor

axios.interceptors.response.use(
  function (response) {
    // console.log(response);
    var token = response.headers.authorization;
    //将token保存到浏览器的localStorage中
    if (token) {
      localStorage.setItem("token", token);
    }
    return response;
  },
  function (error) {
    return Promise.reject(error);
  }
);

// Add a request interceptor
// axios.interceptors.request.use(function (config) {
//   // Do something before request is sent
//   let token = userVerify();

//   !!token && (config.headers.Authorization = `Bearer ${token}`);

//   return config;
// }, function (error) {
//   // Do something with request error
//   return Promise.reject(error);
// });
