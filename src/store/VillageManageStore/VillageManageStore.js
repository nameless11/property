import {
  getVillage,
  setVillage,
  addVillage,
  modifyVillage,
  pagingVillage,
  getinforAll,
} from "../../services/VillageManageServices/VillageManageServices";

export default {
  namespaced: true,
  state: {
    Village: [],
    isLoading: false,
    pagerCount: 11,
    page: 1,
    limit: 3,
    total: 5,
    infos: [],
  },
  getters: {
    parkingTotal(state) {
      let parkingNum = 0;
      state.Village.forEach((item) => {
        parkingNum += item.parkingNumber;
      });
      return parkingNum;
    },
  },
  mutations: {
    // 为了分页传值
    AllVillage(state, payload) {
      state.Village = payload.rows;
    },
    // 普通传值
    setVillage(state, payload) {
      state.Village = payload;
    },
    // loading
    setIsLoading(state, payload) {
      state.isLoading = payload;
    },
    // 分页总条数
    VillageTotals(state, payload) {
      state.total = payload;
    },
    // 当前页数
    setPage(state, payload) {
      state.page = payload;
    },
    // 公告值
    setInfos(state, payload) {
      state.infos = payload;
    },
  },
  actions: {
    // 查询所有
    async fechVillage(context, payload) {
      if (payload) {
        let { page, pageSize } = payload;
        context.commit("setIsLoading", true);
        let resp = await pagingVillage(page, pageSize);
        context.commit("setPage", resp.current);
        context.commit("AllVillage", resp);
        context.commit("VillageTotals", resp.total);
        context.commit("setIsLoading", false);
      } else {
        context.commit("setIsLoading", true);
        let resp = await pagingVillage(context.state.page, context.state.limit);
        context.commit("AllVillage", resp);
        context.commit("VillageTotals", resp.total);
        context.commit("setIsLoading", false);
      }
    },
    // 根据id删除
    async DeleteVillage(context, payload) {
      context.commit("setIsLoading", true);
      await setVillage(payload.id);
      let village = await pagingVillage(
        context.state.page,
        context.state.limit
      );
      // 当前数据数量为0时，页数-1
      if (!village.rows.length) {
        village = await pagingVillage(
          context.state.page - 1,
          context.state.limit
        );
        context.commit("setPage", context.state.page - 1);
      }
      context.commit("VillageTotals", village.total);
      context.commit("setVillage", village.rows);
      context.commit("setIsLoading", false);
    },
    // 新增小区信息
    async addfechVillage(context, payload) {
      console.log(payload);
      let {
        cellname,
        address,
        member,
        Todolist,
        infosComp,
        img,
        parkingNumber,
      } = payload;
      context.commit("setIsLoading", true);
      await addVillage(
        cellname,
        address,
        member,
        Todolist,
        infosComp,
        img,
        parkingNumber
      );
      let village = await pagingVillage(
        context.state.page,
        context.state.limit
      );
      context.commit("setVillage", village.rows);
      context.commit("VillageTotals", village.total);
      context.commit("setIsLoading", false);
    },
    // 搜索内容
    async searchVillage(context, payload) {
      let { type, value } = payload;
      context.commit("setIsLoading", true);
      let resp = await getVillage(type, value);
      context.commit("setVillage", resp.rows);
      context.commit("setIsLoading", false);
    },
    // 修改小区信息
    async ModifyCellVillage(context, payload) {
      context.commit("setIsLoading", true);
      await modifyVillage(payload);
      let resp = await pagingVillage(context.state.page, context.state.limit);
      context.commit("setVillage", resp.rows);
      context.commit("setIsLoading", false);
    },
    // 获取公告信息
    async inforAll(context) {
      context.commit("setIsLoading", true);
      let resp = await getinforAll();
      context.commit("setInfos", resp.data);
      context.commit("setIsLoading", false);
    },
  },
};
