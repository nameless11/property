import {
  deleteManager,
  getManager,
  managerLogin,
  addManager,
  managerPage,
  reviseManager,
  managerVerify,
  loginOut,
} from "@/services/ManagerService/managerService";

export default {
  namespaced: true,
  state: {
    manager: null,
    total: null,
    loading: false,
    personal: null,
    limit: 5,
    page: 1,
  },
  mutations: {
    setManager(state, payload) {
      state.manager = payload;
    },
    setTotal(state, payload) {
      state.total = payload;
    },
    setloading(state, payload) {
      state.loading = payload;
    },
    setPersonal(state, payload) {
      state.personal = payload;
    },
    setPage(state, payload) {
      state.page = payload;
    },
  },
  actions: {
    // 登录
    async fetchManager(context, payload) {
      let { name, pass } = payload;
      let resp = await managerLogin(name, pass);
      console.log(resp);
      context.commit("setManager", resp);
      context.commit("setPersonal", resp.data);
      return resp;
    },
    // 查询渲染
    async GetManager(context, payload) {
      context.commit("setloading", true);
      let { page, pageSize } = payload;
      let resp = await managerPage(page, pageSize);

      context.commit("setTotal", resp.total);
      context.commit("setManager", resp.rows);
      context.commit("setPage", resp.current);
      context.commit("setloading", false);
      return resp;
    },
    //删除
    async delManager(context, id) {
      context.commit("setloading", true);
      let resp = await deleteManager(id);
      let result = await getManager();
      context.commit("setManager", result.rows);
      context.commit("setloading", false);
      return result;
    },
    async AddManager(context, payload) {
      context.commit("setloading", true);
      let { aname, phone, name, pwd, email, VillageManage, image } = payload;
      let resp = await addManager(
        aname,
        phone,
        name,
        pwd,
        email,
        VillageManage,
        image
      );
      let result = await getManager();
      context.commit("setManager", result.rows);
      context.commit("setloading", false);
      return resp;
    },

    // 搜索

    async serchManager(context, payload) {
      context.commit("setloading", true);
      let { type, value } = payload;
      let resp = await getManager(type, value);
      context.commit("setManager", resp.rows);
      context.commit("setTotal", resp.total);
      context.commit("setloading", false);
      return resp;
    },

    //修改
    async ReviseManager(context, payload) {
      context.commit("setloading", true);
      let { reviseId, aname, phone, name, pwd, email, villageManage, images } =
        payload;
      console.log(images);
      let resp = await reviseManager(
        reviseId,
        aname,
        phone,
        name,
        pwd,
        email,
        villageManage,
        images
      );
      let result = await getManager();
      context.commit("setManager", result.rows);
      context.commit("setloading", false);
      return resp;
    },
    async verify(context) {
      context.commit("setloading", true);
      let resp = await managerVerify();
      context.commit("setPersonal", resp.data);
      context.commit("setloading", false);
      return resp.data;
    },

    //注销
    loginOut(context) {
      loginOut();
      context.commit("setPersonal", null);
    },
  },
};
