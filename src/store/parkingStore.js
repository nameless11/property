import {
  getParking,
  deleteUserById,
  addParking,
  modifyParking,
  getPaging,
} from "../service/parkingService";
import { getUser } from "../service/userService";
import { getVillage } from "../service/villageManageService";

export default {
  namespaced: true,
  state: {
    parking: [],
    user: [],
    village: [],
    limit: 5,
    page: 1,
    pagerCount: 7,
    total: 3,
  },
  mutations: {
    setParking(state, payload) {
      state.parking = payload;
    },
    setuser(state, payload) {
      state.user = payload;
    },
    setVillage(state, payload) {
      state.village = payload;
    },
    setTotal(state, payload) {
      state.total = payload;
    },
    setPage(state, payload) {
      state.page = payload;
    },
  },
  actions: {
    //获取全部
    async getParking(context, payload) {
      if (payload) {
        let { page, pageSize } = payload;
        let resp = await getPaging(page, pageSize);
        context.commit("setParking", resp.rows);
        context.commit("setTotal", resp.total);
        context.commit("setPage", resp.current);
        return resp.rows;
      } else {
        let resp = await getPaging(context.state.page, context.state.limit);
        context.commit("setParking", resp.rows);
        context.commit("setTotal", resp.total);
        return resp.rows;
      }
    },
    async getUser(context) {
      let resp = await getUser();
      // console.log(resp.rows);
      context.commit("setuser", resp.rows);
      return resp.rows;
    },
    async getVillage(context) {
      let resp = await getVillage();
      // console.log(resp);
      context.commit("setVillage", resp.rows);
      return resp.rows;
    },
    //删除
    async sssdeleteUserById(context, payload) {
      await deleteUserById(payload);
      let result = await getParking();
      context.commit("setParking", result.rows);
    },
    //新增
    async addparking(context, payload) {
      let { carId, villageManage, user, overdue } = payload;
      await addParking(carId, villageManage, user, overdue);
      let result = await getParking();
      context.commit("setParking", result.rows);
    },
    //查询
    async queryparking(context, payload) {
      let { type, value } = payload;
      let resp = await getParking(type, value);
      context.commit("setParking", resp.rows);
    },
    //修改
    async modifyParking(context, payload) {
      let rsp = await modifyParking(payload);
      let resp = await getParking();
      context.commit("setParking", resp.rows);
    },
  },
};
