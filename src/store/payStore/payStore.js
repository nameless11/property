import { getPay } from "../../services/payService/payService";

export default {
  namespaced: true,
  state: {
    pay: null,
  },
  mutations: {
    setPay(state, payload) {
      state.pay = payload;
    },
  },
  actions: {
    async serchPay(context, payload) {
      if (payload) {
        let { type, value } = payload;
        let resp = await getPay(type, value);
        context.commit("setPay", resp.rows);
      } else {
        let resp = await getPay();
        context.commit("setPay", resp.rows);
      }
    },
  },
};
