import {
  upDateCovidReportById,
  addCovidReport,
  deleteReport,
  searchReports,
} from "@/services/covidReportsService/covidReportsSerive";

export default {
  namespaced: true,
  state: {
    reports: [],
    isLoading: false,
    page: 1,
    pageSize: 10,
    total: 0,
  },
  getters: {
    nomalRportsNum(state) {
      console.log(state.reports);
      let normal = state.reports.filter((item) => item.status == 0);
      console.log(normal);
      return normal.length;
    },
  },
  mutations: {
    setReports(state, payload) {
      state.reports = payload;
    },
    setIsLoading(state, payload) {
      state.isLoading = payload;
    },
    setPage(state, payload) {
      state.page = payload;
    },
    setTotal(state, payload) {
      state.total = payload;
    },
  },
  actions: {
    //搜索、获取全部
    async fetchReports(context, payload) {
      context.commit("setIsLoading", true);
      let pageSize = context.state.pageSize;
      if (payload) {
        pageSize = payload.pageSize;
      }

      let res = await searchReports(context.state.page, payload, pageSize);
      context.commit("setPage", res.data.current);
      context.commit("setTotal", res.data.total);
      console.log(context.state.total);
      context.commit("setReports", res.data.data);
      context.commit("setIsLoading", false);
    },
    async updateReport(context, payload) {
      context.commit("setIsLoading", true);
      await upDateCovidReportById(payload);
      context.commit("setIsLoading", false);
    },
    async addReport(context, payload) {
      context.commit("setIsLoading", true);
      await addCovidReport(payload);
      let res = await searchReports(context.state.page);
      context.commit("setPage", res.data.current);
      context.commit("setTotal", res.data.total);
      context.commit("setReports", res.data.data);
      context.commit("setIsLoading", false);
    },
    async deleteReport(context, payload) {
      context.commit("setIsLoading", true);
      await deleteReport(payload);
      let res = await searchReports(context.state.page);
      context.commit("setPage", res.data.current);
      context.commit("setTotal", res.data.total);
      context.commit("setReports", res.data.data);
      context.commit("setIsLoading", false);
    },
  },
};
