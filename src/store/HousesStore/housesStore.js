import {
  getAllhouses,
  getDeletehouses,
  getAddhouses,
  getQueryhouses,
  getEdithouses,
} from "../../services/HousesService/housesServices";
import { getVillage } from "../../services/VillageManageServices/VillageManageServices";
export default {
  namespaced: true,
  state: {
    isloading: false, //isloading变量，缓冲效果
    houses: [],
    Village: [],
    page: 1, //当前页
    limit: 4, //当前显示多少条
    pageCount: 9, //当前显示多少页码
    total: null, //总共条数
  },
  mutations: {
    setIsLoading(state, payload) {
      state.isloading = payload;
      // console.log(payload);
    },
    setHouses(state, payload) {
      state.houses = payload;
    },
    setVillageManage(state, payload) {
      state.Village = payload;
      // console.log(payload);
    },
    setTotal(state, payload) {
      state.total = payload;
    },
  },
  actions: {
    //渲染所有数据
    async fetchHousesData(context, payload) {
      if (payload) {
        // console.log(payload);
        let { page, pageSize } = payload;
        context.commit("setIsLoading", true);
        let resp = await getAllhouses(page, pageSize);
        // console.log(resp);
        context.commit("setHouses", resp.rows);
        context.commit("setTotal", resp.total);
        context.commit("setIsLoading", false);
      } else {
        context.commit("setIsLoading", true);
        let resp = await getAllhouses(context.state.page, context.state.limit);
        // console.log(resp);
        context.commit("setHouses", resp.rows);
        context.commit("setTotal", resp.total);
        context.commit("setIsLoading", false);
      }
    },
    //删除
    async fetchHousesDelete(context, payload) {
      context.commit("setIsLoading", true);
      // console.log("payload",payload);//payload里包含的id
      await getDeletehouses(payload);
      // context.commit("setHousesDelete", payload)//执行
      context.commit("setIsLoading", false);
    },
    //新增
    async fetchHousesAdd(context, payload) {
      context.commit("setIsLoading", true);
      let resp = await getAddhouses(payload);
      // console.log(resp);
      // context.commit("setHouses", resp.rows);
      context.commit("setIsLoading", false);
      return resp;
    },
    //查询
    async fetchHousesQuery(context, payload) {
      let { type, value } = payload;
      context.commit("setIsLoading", true);
      let resp = await getQueryhouses(type, value);
      context.commit("setHouses", resp.rows);
      context.commit("setIsLoading", false);
    },
    //编辑
    async fetchHousesEdit(context, payload) {
      // console.log(payload);
      context.commit("setIsLoading", true);
      await getEdithouses(payload);
      context.commit("setIsLoading", false);
    },
    //获取小区所有数据
    async fetchVillageManage(context) {
      context.commit("setIsLoading", true);
      let resp = await getVillage();
      // console.log(resp.rows);
      context.commit("setVillageManage", resp.rows);
      context.commit("setIsLoading", false);
    },
  },
};
