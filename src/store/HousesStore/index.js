import Vue from 'vue'
import Vuex from 'vuex'
import housesStore from "./housesStore";
import VillageManageStore from '../VillageManageStore/VillageManageStore'

Vue.use(Vuex);

let store = new Vuex.Store({
    modules: {//分割成模块
      VillageManageStore,   housesStore
    }
})


export default store
