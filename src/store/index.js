import Vue from "vue";
import Vuex from "vuex";
import managerStore from "./managerStore";
import userStore from "./userStore";
import housesStore from "./HousesStore/housesStore";
import VillageManageStore from "./VillageManageStore/VillageManageStore";
import covidReportsStore from "@/store/covidReportsStore/covidReportsStore";
import infosStore from "@/store/infosStore/infosStore";
import parkingStore from "./parkingStore";
import payStore from "./payStore/payStore";
import repairStore from "./repairStore/repairStore";
import forumStore from "./forumStore/forumStore";
import complaintStore from "./complaintStore/complaintStore"

Vue.use(Vuex);

let store = new Vuex.Store({
  modules: {
    managerStore,
    userStore,
    VillageManageStore,
    housesStore,
    covidReportsStore,
    parkingStore,
    infosStore,
    repairStore,
    payStore,
    forumStore,
    complaintStore
  },
});

export default store;
