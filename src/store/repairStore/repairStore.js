import {
  repairPage,
  deleteRepair,
  getRepair,
  addRepair,
  reviseRepair,
} from "@/services/RepairService/repairService";

export default {
  namespaced: true,
  state: {
    repair: [],
    loading: false,
    limit: 5,
    page: 1,
  },
  getters: {
    repairTotal(state) {
      let undo = state.repair.filter((item) => item.state == 0);
      return undo.length;
    },
  },
  mutations: {
    setRepair(state, payload) {
      state.repair = payload;
    },
    setloading(state, payload) {
      state.loading = payload;
    },
    setPage(state, payload) {
      state.page = payload;
    },
  },
  actions: {
    // 查询渲染
    async GetRepair(context, payload) {
      context.commit("setloading", true);
      let { page, pageSize } = payload;
      let resp = await repairPage(page, pageSize);

      // context.commit('setTotal', resp.total)
      context.commit("setRepair", resp.rows);
      context.commit("setPage", resp.current);
      context.commit("setloading", false);
      return resp;
    },
    //删除
    async delRepair(context, id) {
      context.commit("setloading", true);
      let resp = await deleteRepair(id);
      let result = await getRepair();
      context.commit("setRepair", result.rows);
      context.commit("setloading", false);
      return result;
    },
    async AddRepair(context, payload) {
      context.commit("setloading", true);
      let { VillageManage, area, titlle, content, images, creatDate } = payload;
      let resp = await addRepair(
        VillageManage,
        area,
        titlle,
        content,
        images,
        creatDate
      );
      let result = await getRepair();
      context.commit("setRepair", result.rows);
      context.commit("setloading", false);
      return resp;
    },

    // 搜索

    async serchRepair(context, payload) {
      context.commit("setloading", true);
      let { type, value } = payload;
      let resp = await getRepair(type, value);
      context.commit("setRepair", resp.rows);
      // context.commit('setTotal', resp.total)
      context.commit("setloading", false);
      return resp;
    },

    //修改
    async ReviseRepair(context, payload) {
      context.commit("setloading", true);
      let { chooseId, value } = payload;
      let resp = await reviseRepair(chooseId, value);
      let result = await getRepair();
      context.commit("setRepair", result.rows);
      context.commit("setloading", false);
      return resp;
    },
  },
};
