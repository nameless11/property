import {
  getInfos,
  postInfo,
  deleteInfoById,
  updateInfoById,
} from "@/services/infosSerivce/InfosSerive";

export default {
  namespaced: true,
  state: {
    infos: [],
    current: 1,
    pageSize: 10,
    total: 0,
    isLoading: false,
  },
  getters: {
    randomInfo(state) {
      let random = state.infos[Math.floor(Math.random() * state.infos.length)];
      return random;
    },
  },
  mutations: {
    setInfos(state, payload) {
      state.infos = payload;
    },
    setCurrent(state, payload) {
      state.current = payload;
    },
    setTotal(state, payload) {
      state.total = payload;
    },
    setIsLoading(state, payload) {
      state.isLoading = payload;
    },
  },
  actions: {
    async fetchInfos(context, payload) {
      context.commit("setIsLoading", true);
      let res = await getInfos(context.state.current, payload);
      context.commit("setInfos", res.data.data);
      context.commit("setCurrent", res.data.page);
      context.commit("setTotal", res.data.total);
      context.commit("setIsLoading", false);
    },
    async addInfo(context, payload) {
      context.commit("setIsLoading", true);
      await postInfo(payload);
      let res = await getInfos(context.state.current);
      context.commit("setInfos", res.data.data);
      context.commit("setCurrent", res.data.page);
      context.commit("setTotal", res.data.total);
      context.commit("setIsLoading", false);
    },
    async deleteInfo(context, payload) {
      context.commit("setIsLoading", true);
      await deleteInfoById(payload);
      let res = await getInfos(context.state.current);
      context.commit("setInfos", res.data.data);
      context.commit("setCurrent", res.data.page);
      context.commit("setTotal", res.data.total);
      context.commit("setIsLoading", false);
    },
    async updateInfo(context, payload) {
      context.commit("setIsLoading", true);
      await updateInfoById(payload._id, payload);
      let res = await getInfos(context.state.current);
      context.commit("setInfos", res.data.data);
      context.commit("setCurrent", res.data.page);
      context.commit("setTotal", res.data.total);
      context.commit("setIsLoading", false);
    },
  },
};
