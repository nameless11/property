import { addUser, deleteUser, getUser, reviseUser, userPage } from "@/services/UserService/userService"

export default {
    namespaced: true,
    state: {
        user: null,
        total: null,
        loading: false,
        limit:5,
        page:1
    },
    mutations: {
        setUser(state, payload) {
            state.user = payload
        },
        setTotal(state, payload) {
            state.total = payload
        },
        setloading(state, payload) {
            state.loading = payload
        },
        setPage(state, payload) {
            state.page = payload
        }
    },
    actions: {

        async fetchUser(context, payload) {
            context.commit('setloading', true)
            let { page, pageSize } = payload;
            let resp = await userPage(page, pageSize);
            context.commit('setUser', resp.rows)
            context.commit('setTotal', resp.total)
            context.commit('setPage', resp.current)
            context.commit('setloading', false)
            return resp
        },

        async delUser(context, id) {
            context.commit('setloading', true)
            let resp = await deleteUser(id)
            let result = await getUser();
            context.commit('setUser', result.rows)
            context.commit('setloading', false)
            return result
        },
        async AddUser(context, payload) {
            context.commit('setloading', true)
            let { aname, phone, username, email, houses } = payload
            let resp = await addUser(aname, phone, username, email, houses)
            let result = await getUser();
            context.commit('setUser', result.rows)
            context.commit('setloading', false)
            return resp
        },
        //查询
        async serchUser(context, payload) {
            context.commit('setloading', true)
            let { type, value } = payload
            let resp = await getUser(type, value)
            context.commit('setUser', resp.rows)
            context.commit('setTotal', resp.total)
            context.commit('setloading', false)
            return resp
        },

        //修改
        async ReviseUser(context,payload) {
            context.commit('setloading', true)
            let resp = await reviseUser(payload)
            let result = await getUser();
            context.commit('setUser', result.rows)
            context.commit('setloading', false)
            return resp
        },

    }
}