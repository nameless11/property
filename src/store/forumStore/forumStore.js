import {
  getForunPosts,
  deletePostsById,
} from "@/services/forumPosts/forumPsotsSerivce";

export default {
  namespaced: true,
  state: {
    posts: [],
    current: 1,
    pageSize: 10,
    total: 0,
    isLoading: false,
  },
  mutations: {
    setPosts(state, payload) {
      state.posts = payload;
    },
    setCurrent(state, payload) {
      state.current = payload;
    },
    setTotal(state, payload) {
      state.total = payload;
    },
    setIsLoading(state, payload) {
      state.isLoading = payload;
    },
  },
  actions: {
    async fetchPosts(context, payload) {
      context.commit("setIsLoading", true);
      let res = await getForunPosts(context.state.current, payload);
      context.commit("setPosts", res.data.data);
      context.commit("setCurrent", res.data.page);
      context.commit("setTotal", res.data.total);
      context.commit("setIsLoading", false);
    },
    async deletePost(context, payload) {
      context.commit("setIsLoading", true);
      await deletePostsById(payload);
      let res = await getForunPosts(context.state.current);
      context.commit("setPosts", res.data.data);
      context.commit("setCurrent", res.data.page);
      context.commit("setTotal", res.data.total);
      context.commit("setIsLoading", false);
    },
  },
};
