import axios from "axios";

export async function getInfos(page, searchParams) {
  let res = await axios.get("/api/infos", {
    params: {
      current: page,
      searchParams,
    },
  });
  return res;
}

export async function deleteImage(img) {
  await axios.delete("/api/infos/images/" + img);
}

export async function postInfo(info) {
  await axios.post("/api/infos", info);
}

export async function deleteInfoById(id) {
  await axios.delete("/api/infos/" + id);
}

export async function updateInfoById(id, payload) {
  await axios.put("/api/infos/" + id, payload);
}
