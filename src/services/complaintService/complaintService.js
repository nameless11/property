import axios from 'axios';

export async function getRepair(type, value) {
    let resp = await axios.get('/api/complaint/', {
        params: {
            type,
            value
        }
    });
    return resp.data
}

export async function deleteRepair(_id) {
    let resp = await axios.delete('/api/complaint/' + _id, {
    });
    return resp.data
}
//修改
export async function reviseRepair( chooseId, value) {
    let resp = await axios.put('/api/complaint/' + chooseId, {
        chooseId,
        state:value

    });
    return resp.data
}


// 分页
export async function repairPage(page, pageSize) {
    let resp = await axios.get('/api/complaint/', {
        params: {
            current: page,
            pageSize
        }
    });

    return resp.data
}



