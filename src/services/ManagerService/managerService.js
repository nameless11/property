import axios from "axios";

export async function managerLogin(name, pwd) {
  let resp = await axios.post("/api/manager/login", {
    name,
    pwd,
  });
  console.log(resp);
  return resp.data;
}
export async function getManager(type, value, page, pageSize) {
  let resp = await axios.get("/api/manager/", {
    params: {
      type,
      value,
      current: page,
      pageSize,
    },
  });

  return resp.data;
}
export async function deleteManager(_id) {
  let resp = await axios.delete("/api/manager/" + _id, {});
  return resp.data;
}
export async function addManager(
  aname,
  phone,
  name,
  pwd,
  email,
  VillageManage,
  image
) {
  let resp = await axios.post("/api/manager/reg", {
    aname,
    phone,
    name,
    pwd,
    email,
    VillageManage,
    images: image,
  });
  return resp.data;
}

// 分页
export async function managerPage(page, pageSize) {
  let resp = await axios.get("/api/manager/", {
    params: {
      current: page,
      pageSize,
    },
  });

  return resp.data;
}
//修改
export async function reviseManager(
  reviseId,
  aname,
  phone,
  name,
  pwd,
  email,
  villageManage,
  images
) {
  console.log(images);
  let resp = await axios.put("/api/manager/" + reviseId, {
    reviseId,
    aname,
    phone,
    name,
    pwd,
    email,
    VillageManage: villageManage,
    images,
  });
  return resp.data;
  // let resp = await axios({
  //     method: 'put',
  //     url: '/api/manager/' + reviseId,
  //     data: {
  //         reviseId,
  //         aname,
  //         phone,
  //         name,
  //         pwd,
  //         email,
  //         VillageManage: villageManage
  //     }
  // })
  // return resp.data
}
export async function managerVerify() {
  let resp = await axios.get("/api/manager/verify/whoami");
  return resp.data;
}

//注销
export function loginOut() {
  localStorage.removeItem("token");
}
