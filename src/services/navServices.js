import axios from "axios";

export async function getAllNavs() {
  let resp = await axios.get("/api/navs/");
  return resp.data;
}

export async function deleteNavs(_id) {
  let resp = await axios.delete("/api/navs/" + _id);
  return resp.data;
}

export async function addTB(form) {
  let { icon, name, createTime, url } = form;
  let resp = await axios.post("/api/navs", {
    icon,
    name,
    createTime,
    url,
  });
  return resp.data;
}

export async function gaiTB(form1) {
  let { _id, icon, name, createTime, url } = form1;
  let resp = await axios.put("/api/navs/" + _id, {
    icon,
    name,
    createTime,
    url,
  });
  return resp.data;
}
