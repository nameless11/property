import axios from 'axios'

// 查询所有小区信息
export async function getVillage(type, value, page, pageSize) {
  let resp = await axios.get('/api/VillageManage/', {
    params: {
      type,
      value,
      current: page,
      pageSize
    }
  })
  return resp.data
}

// 根据 id 删除小区信息
export async function setVillage(id) {
  let resp = await axios.delete('/api/VillageManage/' + id)
  console.log(resp.data);
  return resp.data
}

// 新增小区信息
export async function addVillage(cellname, address, member, Todolist, infosComp, img, parkingNumber) {
  let resp = await axios.post('/api/VillageManage/', {
    cellname, address, member, Todolist, infos: infosComp, img,parkingNumber
  })
  return resp.data
}

// 修改小区信息
export async function modifyVillage(value) {
  let { address, cellname, member, id, Todolist, infos, img, parkingNumber } = value
  console.log(infos);
  let resp = await axios.put('/api/VillageManage/' + id, {
    address, cellname, member, Todolist, infos, img,parkingNumber
  })
  return resp.data
}

// 查询所有小区信息分页
export async function pagingVillage(page, pageSize) {
  let resp = await axios.get('/api/VillageManage/', {
    params: {
      current: page,
      pageSize
    }
  })
  return resp.data
}

// 查询公告所有
export async function getinforAll(page, pageSize) {
  let resp = await axios.get('/api/infos/', {
    params: {
      current: page,
      pageSize
    }

  })
  return resp.data
}


