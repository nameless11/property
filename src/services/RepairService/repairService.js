import axios from 'axios';

export async function getRepair(type, value) {
    let resp = await axios.get('/api/repair/', {
        params: {
            type,
            value
        }
    });
    return resp.data
}

export async function deleteRepair(_id) {
    let resp = await axios.delete('/api/repair/' + _id, {
    });
    return resp.data
}
//修改
export async function reviseRepair( chooseId, value) {
    let resp = await axios.put('/api/repair/' + chooseId, {
        chooseId,
        state:value

    });
    return resp.data
}

export async function addRepair( VillageManage,area, titlle, content,images,creatDate) {
    let resp = await axios.post('/api/repair/reg', {
        VillageManage,
        area,
        titlle,
        content,
        images,
        creatDate
    });
    return resp.data
}

// 分页
export async function repairPage(page, pageSize) {
    let resp = await axios.get('/api/repair/', {
        params: {
            current: page,
            pageSize
        }
    });

    return resp.data
}



