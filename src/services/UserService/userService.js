import axios from 'axios';

export async function getUser(type, value) {
    let resp = await axios.get('/api/users/', {
        params: {
            type,
            value
        }
    });
    return resp.data
}

export async function deleteUser(_id) {
    let resp = await axios.delete('/api/users/' + _id, {
    });
    return resp.data
}
//修改
export async function reviseUser(value) {
    let { reviseId, aname, phone } = value
    let resp = await axios.put('/api/users/' + reviseId, {
        reviseId,
        aname,
        phone
    });
    return resp.data
}

export async function addUser(aname, phone, username, email, houses) {
    let resp = await axios.post('/api/users/reg', {
        aname,
        phone,
        username,
        email,
        houses
    });
    return resp.data
}

// 分页
export async function userPage(page, pageSize) {
    let resp = await axios.get('/api/users/', {
        params: {
            current: page,
            pageSize
        }
    });

    return resp.data
}



