import axios from "axios";

export async function getForunPosts(current, searchParams) {
  let res = await axios.get("/api/forum", {
    params: {
      current,
      searchParams,
    },
  });
  console.log(res);
  return res;
}

export async function deletePostsById(id) {
  await axios.delete("/api/forum/" + id);
}
