import axios from "axios";

//查询所有//分页
export async function getAllhouses(page, pageSize) {
    let resp = await axios.get("/api/houses", {
        params: {//get方法有的params
            current: page,
            pageSize,
        }
    });
    return resp.data;
}

//新增
export async function getAddhouses({ roomNumber, floor, status, building, unit, Village, images }) {
    let resp = await axios.post("/api/houses/", {
        roomNumber, floor, status, building, unit, VillageManage: Village, images
    });

    return resp.data;
}
//删除
export async function getDeletehouses(_id) {
    let resp = await axios.delete("/api/houses/" + _id);
    // console.log(resp);
    return resp.data;
}

//查询
export async function getQueryhouses(type, value, page, pageSize) {
    let resp = await axios.get("/api/houses/", {
        params: {//get方法有的params
            current: page,
            pageSize,
            type,
            value
        }
    });
    return resp.data;
}

//编辑
export async function getEdithouses(Object) {
    let { id, roomNumber, floor, status, building, unit, VillageManage, images } = Object;
    let resp = await axios.put("/api/houses/" + id, {
        roomNumber, floor, status, building, unit, VillageManage, images
    })

    return resp.data;
}




