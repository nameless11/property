import axios from "axios";

export async function upDateCovidReportById(report) {
  let res = await axios.put("/api/covidReports/" + report._id, report);
  console.log(res);
}

export async function addCovidReport(report) {
  let res = await axios.post("/api/covidReports", report);
  console.log(res);
}

export async function deleteReport(id) {
  await axios.delete("/api/covidReports/" + id);
}

export async function searchReports(page, searchParams, pageSize) {
  let res = await axios.get("/api/covidReports", {
    params: { searchParams, current: page, pageSize },
  });
  return res;
}
