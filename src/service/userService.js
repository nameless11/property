import axios from 'axios';

export async function getUser(type,value,page, pageSize) {
    let resp = await axios.get('/api/users/', {
        params: {
            type,
            value,
            current:page,
            pageSize
        }
    });
    // console.log(resp);
    return resp.data
}

export async function deleteUser(_id) {
    let resp = await axios.delete('/api/users/' + _id, {
    });
    return resp.data
}
export async function addUser(aname, phone, username, email) {
    let resp = await axios.post('/api/users/reg', {
        aname,
        phone,
        username,
        email
    });
    return resp.data
}
