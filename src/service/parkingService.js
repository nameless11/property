import axios from 'axios'


export async function deleteUserById(_id) {
    let resp = await axios.delete('/api/parking/' + _id)
    return resp.data
  }

  export async function findById(_id) {
    let resp = await axios.findById('/api/parking/' + _id)
    return resp.data
  }

  export async function getParking(type,value,page, pageSize) {
    let resp = await axios.get("/api/parking/", {
      params: {
        type,
        value,
        pageSize,
        current: page
      }
    });
    return resp.data; 
  }

  export async function getPaging(page, pageSize) {
    let resp = await axios.get("/api/parking/", {
      params: {
     
        pageSize,
        current: page
      }
    });
    return resp.data; 
  }

  // 新增
export async function addParking(carId, villageManage,user,overdue) {
  let resp = await axios.post('/api/parking/', {
   carId, villageManage,user,overdue
  })
  return resp.data
}


//修改
export async function modifyParking(value){
 let{carId, villageManage,user,overdue,id}=value;
 let resp = await axios.put('/api/parking/'+id, {
  carId, villageManage,user,overdue
 })
 return resp.data
}