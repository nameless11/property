import axios from 'axios'

// 查询所有小区信息
export async function getVillage(type,value,page, pageSize) {
  let resp = await axios.get('/api/VillageManage/', {
    params: {
      type,
      value,
      current: page,
      pageSize
    }
  })
  // console.log(resp.data);
  return resp.data
}

// 根据 id 删除小区信息
export async function setVillage(id) {
  let resp = await axios.delete('/api/VillageManage/'+id)
  console.log(resp.data);
  return resp.data
}

// 新增小区信息
export async function addVillage( cellname, address, member) {
  let resp = await axios.post('/api/VillageManage/', {
     cellname, address, member
    //  Todolist, notice
  })
  // console.log(resp.data);
  return resp.data
}