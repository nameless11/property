import Vue from "vue";
import App from "./App.vue";
import ElementUI from "element-ui"; //用到Element需要引入
import "element-ui/lib/theme-chalk/index.css";
import VueRouter from "vue-router";
import store from "./store";
import config from "@/routes/config";
import * as echarts from "echarts";

import "./utils/axios.intercepter";

Vue.use(ElementUI);
Vue.use(VueRouter);

Vue.config.productionTip = false;
const router = new VueRouter(config);

//获取疫情报备数据
const originalPush = VueRouter.prototype.push;

VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err);
};

//全局导航守卫
router.beforeEach(function (to, from, next) {
  if (to.meta.auth) {
    if (store.state.managerStore.loading) {
      next({ name: "Auth", params: { path: to.path } });
    } else if (store.state.managerStore.personal) {
      next(); //允许进入
    } else {
      next({ name: "Login" });
    }
  } else {
    next();
  }
});

//echarts
Vue.prototype.$echarts = echarts;

store.dispatch("VillageManageStore/fechVillage");
store.dispatch("VillageManageStore/inforAll");
store.dispatch("payStore/serchPay");

store.dispatch("parkingStore/getParking");
store.dispatch("parkingStore/getVillage");
store.dispatch("parkingStore/getUser");
store.dispatch("covidReportsStore/fetchReports", { pageSize: 9999 });

store.dispatch("managerStore/verify");
new Vue({
  render: (h) => h(App),
  router,
  store,
}).$mount("#app");
