//url路径--->views地址，那个路径对应的那个组件
export default {
  //导出
  mode: "history",
  routes: [
    {
      path: "/login",
      name: "Login",
      component: () => import("@/components/LoginComp"),
    },
    {
      path: "/",
      name: "Home",
      component: () => import("@/views/HomeView"),
      children: [
        {
          path: "",
          name: "Shouye",
          component: () => import("@/components/ShouyeComp"),
        },
        {
          path: "/User",
          name: "User",
          component: () => import("@/components/users/UserComp"),
          meta: {
            auth: true,
          },
        },
        {
          path: "/Manager",
          component: () => import("@/components/manager/ManagerComp"),
          meta: {
            auth: true,
          },
        },
        {
          path: "/houses",
          name: "Houses",
          component: () => import("@/components/houses/HousesComp"),
          meta: {
            auth: true,
          },
        },
        {
          path: "village",
          name: "Village",
          component: () => import("@/components/VillageManage/VillageManage"),
          meta: {
            auth: true,
          },
        },
        {
          path: "covidReports",
          name: "CovidReports",
          component: () => import("@/views/CovidReportsView"),
          meta: {
            auth: true,
          },
        },
        {
          path: "/personal",
          name: "Personal",
          component: () => import("@/components/PersonalComp"),
          //
          meta: {
            auth: true,
          },
        },
        {
          path: "nav",
          name: "Nav",
          component: () => import("@/components/NavsComp"),
          meta: {
            auth: true,
          },
        },
        {
          path: "infos",
          name: "Infos",
          component: () => import("@/views/InfosView"),
          meta: {
            auth: true,
          },
        },
        {
          path: "/parking",
          name: "Parking",
          component: () => import("@/components/parkingComp"),
          meta: {
            auth: true,
          },
        },
        {
          path: "/pay",
          name: "Pay",
          component: () => import("@/components/pay/pay"),
          meta: {
            auth: true,
          },
        },
        {
          path: "/repair",
          name: "Repair",
          component: () => import("@/components/repair/repairComp"),
          meta: {
            auth: true,
          },
        },
        {
          path: "/forum",
          name: "Forum",
          component: () => import("@/components/forumPosts/forumPostsComp"),
          meta: {
            auth: true,
          },
        },
        {
          path: "/complaint",
          name: "Complaint",
          component: () => import("@/components/complaint/complaintComp"),
          meta: {
            auth: true,
          },
        },
      ],
    },

    {
      path: "/personal",
      name: "Personal",
      component: () => import("@/components/PersonalComp"),
      //
      meta: {
        auth: true,
      },
    },

    {
      path: "/auth",
      name: "Auth",
      component: () => import("@/components/AuthComp"),
    },
  ],
};
